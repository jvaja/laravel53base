@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if(count($albums)>0)
                <div class="panel panel-default">
                    <div class="panel-heading">Album List</div>
                    <div class="panel-body">
                        <table class="table"> 
                            <thead> 
                                <tr> 
                                    <th>#</th> 
                                    <th>Name</th> 
                                    <th>Created On</th> 
                                    <th>Action</th> 
                                </tr> 
                            </thead> 
                            <tbody> 
                                @foreach($albums as $album)
                                    <tr> 
                                        <th scope="row">{{ $album->id }}</th> 
                                        <td> {{ $album->name }} </td> 
                                        <td>{{ $album->created_at }}</td> 
                                        <td>
                                            <a href="album/{{ $album->id }}/edit" title="View">Edit</a> | 
                                            <a href="album/{{ $album->id }}/destroy" title="View">Remove</a>
                                        </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
               <div class="panel panel-default">
                    <div class="panel-heading">Create a new album!!</div>
                </div> 
            @endif
        </div>
    </div>
</div>
@endsection
