@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Album</div>
                <div class="panel-body">
                    @if(session('message'))
                        <p class="alert {{ session('alert-class', 'alert-info') }}">{{ session('message') }}</p>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/album/update') }}">
                        {{ method_field('PUT') }}
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Album Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $album->name }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Album Description</label>

                            <div class="col-md-6">
                                <textarea id="description" name="description" class="form-control" rows="5">{{ $album->description }} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="status" class="col-md-4 control-label">Select list:</label>
                          <div class="col-md-6">
                              <select name="status" class="form-control" id="status">
                                <option value="draft" {{ ($album->status=='draft')?'selected':''  }}>Draft</option>
                                <option value="publish" {{ ($album->status=='publish')?'selected':''  }}>published</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="id" value="{{ $album->id }}">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                                <a href='{{ url("/picture/$album->id/album")}}' class="btn btn-primary">
                                    Add Picture
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
