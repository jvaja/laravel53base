<?php

use Illuminate\Database\Seeder;

class HotelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('hotels')->insert(["name"=>str_random(10),"location"=>str_random(10),"wifi"=>0,"allow_pets"=>0]);
    }
}
