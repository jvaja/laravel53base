<?php 

if ( ! function_exists('getPhone'))
{
    function getPhone($phone_number,$num=10)
	{
	  if($phone_number!=''){
		  // $res=substr(preg_replace("/[^0-9]/", "", $phone_number),-$num);
		  $res=preg_replace("/[^0-9]/", "", $phone_number);
		  if($res){
		  	return $res;
		  }
		  else{
		  	return Hash::make(str_random(16).time().str_random(3));	
		  }
	  }
	  return '';
	}
}
if ( ! function_exists('randomInteger')){
	function randomInteger($num=6){
		$value=1;
		$start=str_pad($value, $num, '0', STR_PAD_RIGHT);
		$end=($start*10) - 1;
		return mt_rand($start, $end);
	}
}
if ( ! function_exists('generateRandomString'))
{
	function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
	      $randomString .= $characters[rand(0, strlen($characters) - 1)];
	  	}
	  	return $randomString;
	}
}
if(! function_exists('clean')){
	function clean($string) {
	   $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
}
/**
 * 07-04-2015 : get pure hash tags from a string
 */
if(! function_exists('convertHashtags')){
	function convertHashtags($str){
		preg_match_all('/#([^\s]+)/', $str, $matches);
		$hashtags = $matches[1];
		$hashtags=array_map('stripslashes',$hashtags);
		return $hashtags;
	}
}
if(! function_exists('hashAdder')){
	function hashAdder($array,$vals='#'){
		if(is_array($array)){
			if(count($array)>0){
				$arr=array_map(function($value)use($vals) { return $vals."$value"; }, $array);
				return implode(' ',$arr);
			}			
		}
		return '';
	}
}

if(! function_exists('removeQuotes')){
	function removeQuotes($str){
		if(is_string($str)){
			return trim($str,'"');
		}
		return '';
	}
}

if(! function_exists('s3StrTrim')){
	function s3StrTrim($str){
		if(is_string($str)){
			preg_match('|https?://[^/\r\n]+/([^\r\n]*)?|',$str,$mathces);
			if(count($mathces)>0){
				return $mathces[1];
			}
			return '';			
		}
		return '';
	}
}
if(! function_exists('getUXtime')){
	function getUXtime($req){
		$req=str_replace('/','-', $req);
		$explodeDate=explode("-", $req);
		$date=$explodeDate[1];
		$month=$explodeDate[0];
		$year=$explodeDate[2];
		$strTime=$year.'-'.$month.'-'.$date."T00:00:00+00:00";
		return (string)strtotime($strTime);
	}
}
if(! function_exists('camelCase')){
	function camelCase($str){
		$newStr=ucwords(strtolower($str));
		return (string)$newStr;
	}
}
?>