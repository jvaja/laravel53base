<?php

if ( ! function_exists( 'mpr' ) ) {
	function mpr( $d ) {
		echo '<pre>'.print_r( $d, true ).'</pre>';
	}
}

if ( ! function_exists( 'mprd' ) ) {
	function mprd( $d ) {
		mpr( $d );
		die;
	}
}

if ( ! function_exists( 'mvr' ) ) {
	function mvr( $d ) {
		echo '<pre>'.var_dump( $d, true ).'</pre>';
	}
}

if ( ! function_exists( 'mvrd' ) ) {
	function mvrd( $d ) {
		mvr( $d );
		die;
	}
}
if ( ! function_exists( 'array_pluck' ) ) {
	function array_pluck( $arr, $toPlucked ) {
		return array_map( function( $arr ) use( $toPlucked ) {
				return $arr["$toPlucked"];
			}, $arr );
	}
}
if ( ! function_exists( 'array_internal_merge' ) ) {
	function array_internal_merge( $array ) {
		$flatten = array();
	    array_walk_recursive($array, function($value) use(&$flatten) {
	        $flatten[] = $value;
	    });

	    return array_unique($flatten);
	}
}
if ( ! function_exists( 'permutation_array' ) ) {
	function permutation_array( $arr, $arr_onedimension, $key ) {
		$arr3=array();
		foreach ( $arr_onedimension as $val ) {
			$arr3=array_merge( $arr3, array_pluck_permutation( $arr, $val, $key ) );
		}
		return $arr3;
	}

}
if ( ! function_exists( 'array_pluck_permutation' ) ) {
	function array_pluck_permutation( $arr, $val, $key ) {
		return array_map( function( $arr ) use( $val, $key ) {
				return array_push_assoc( $arr, $val, $key ) ;
			}, $arr );
	}
}
if ( ! function_exists( 'array_push_assoc' ) ) {
	function array_push_assoc( $array, $value, $key ) {
		$array["$key"] = $value;
		return $array;
	}
}
if ( ! function_exists( 'jSearch' ) ) {
	function jSearch( $array, $key, $value ) {
		$results = array();

		if ( is_array( $array ) ) {
			if ( isset( $array[$key] ) && $array[$key] == $value ) {
				$results[] = $array;
			}

			foreach ( $array as $subarray ) {
				$results = array_merge( $results, jSearch( $subarray, $key, $value ) );
			}
		}

		return $results;
	}

}
if ( ! function_exists( 'getDuration' ) ) {
	function getDuration( $filepath ) {
		// execute ffmpeg form linux shell and grab duration from output
		// $result = shell_exec( "ffmpeg -i ".$filepath.' 2>&1 | grep -o \'Duration: [0-9:.]*\'' );
		$result = shell_exec( "ffmpeg -i ".$filepath.' 2>&1 | grep -o \'Duration: [0-9:.]*\'' );
		$duration = str_replace( 'Duration: ', '', $result ); // 00:05:03.25

		//get the duration in seconds
		$timeArr = preg_split( '/:/', str_replace( 's', '', $duration ) );
		$t  = ( ( $timeArr[2] )? $timeArr[2]*1 + $timeArr[1] * 60 + $timeArr[0] * 60 * 60 : $timeArr[1] + $timeArr[0] * 60 );

		return $t;
	}
}
if ( ! function_exists( 'importMeshSortArr' ) ) {
	function importMeshSortArr( $a, $b ) {
		if ( $a["priority"] == $b["priority"] ) {
			return strtotime( $a["created_at"] ) - strtotime( $b["created_at"] );
		}
		return $a["priority"] - $b["priority"];
	}
}

if(! function_exists('mediaDuration')){
	function mediaDuration($filepath){
		$t = shell_exec("avconv -i ".$filepath." 2>&1 | grep 'Duration' | awk '{print $2}' | sed s/,//");
		if(!empty($t)){
			return $t;
		}
		return '';
	}
}
if ( ! function_exists( 'sortArrByCreatedDate' ) ) {
	function sortArrByCreatedDate( $a, $b ) {
		// if ( $a["priority"] == $b["priority"] ) {
			return strtotime( $a["created_at"] ) - strtotime( $b["created_at"] );
		// }
		// return $a["priority"] - $b["priority"];
	}
}
if ( ! function_exists( 'arr_refine' ) ) {
	function arr_refine( $d ) {
		return array_values(array_filter($d));
	}
}
?>
