<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hotel;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Hotel $hotel)
    {
        $this->middleware('auth');
        $this->hotel = $hotel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function search(Request $request){
        $filterOn=$request->get('filterOn');
        if($filterOn){
            $queryFilter=$request->get('filter');
            extract($queryFilter);
            $records=$this->hotel->orWhere('name', 'like', "%$text_query%");
            $finalRecord=$records->paginate(10);
            return $finalRecord;
        }
        else{
            return Hotel::paginate(10);
        }
    }
}
