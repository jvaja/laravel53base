<?php 
namespace App\MyTraits;

use Validator;
use Illuminate\Support\Facades\Storage;
use Image;
trait FileuploadTrait{

	public function localUpload($destination_folder,$file){
		$path = $file->store($destination_folder);
        return $path;
	}
	public function localUploadImg($destination_folder,$file){
		$path = $file->store($destination_folder);
        return $path;
	}

	public function resizeImg($storage=''){
		$pathInformation = pathinfo($storage);
		// open and resize an image file
		$dirname=$pathInformation['dirname'];
		$basename=$pathInformation['basename'];
		$extension=$pathInformation['extension'];
		$filename=$pathInformation['filename'];

		$sourceFile='storage/app/'.$dirname.'/'.$basename;
		$destinationFile= 'storage/app/'.$dirname.'/'.$filename.'_thumb.'.$extension;
		
		$img = Image::make($sourceFile)->resize(300, 200);
		// save the same file as jpeg with default quality
		$img->save($destinationFile);
	}
	public function checkIsValideImage($files){
		foreach ($files as $file) {
		    $rules = array('file' => 'required|image'); 
		    $validator = Validator::make(array('file'=> $file), $rules);
		    if ($validator->fails()) {
		    	return 400;
		    	break;
		    }
		}
	}
	public function dltFile($storage){
		$pathInformation = pathinfo($storage);
		// open and resize an image file
		$dirname=$pathInformation['dirname'];
		$basename=$pathInformation['basename'];
		$extension=$pathInformation['extension'];
		$filename=$pathInformation['filename'];

		$sourceFile=$dirname.'/'.$basename;
		Storage::delete($sourceFile);
	}
}
