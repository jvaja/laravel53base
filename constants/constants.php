<?php 
	/*define('_PATH', substr(dirname(__FILE__),0,-25));
	define('_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(_PATH))));
	$smProtocol = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
	define('DOMAIN_URL', $smProtocol.$_SERVER['SERVER_NAME']);*/
	define('PROJECT_FOLDER','zwp');
	// define('APPLICATION_URL',DOMAIN_URL.'/'.PROJECT_FOLDER);
	define("DOC_ROOT",$_SERVER['DOCUMENT_ROOT']);
	define("ADMIN_MAIL_SENDER_EMAIL","");
	define("ADMIN_MAIL_SENDER_NAME","");

	define('ASSET_LOCATION',"public/");
	define('DIST_LOCATION',ASSET_LOCATION.'');
	define('BOOTSTRAP_PATH',DIST_LOCATION.'bootstrap/');

	define('DIST_CSS',DIST_LOCATION.'css/');
	define('DIST_JS',DIST_LOCATION.'js/');
	define('DIST_IMG',DIST_LOCATION.'img/');
	define('PLUGINS',ASSET_LOCATION.'plugins/');
?>