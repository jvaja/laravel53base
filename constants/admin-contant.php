<?php 
	define('ADMIN_MAINTITLE',"Thor");
	// ADMIN ASSETS MANAGER
	// define('ADMIN_URL',APPLICATION_URL.'/admin');
	define('ADM_ASSET_LOCATION',"public/admin/assets/");
	define('ADM_DIST_LOCATION',ADM_ASSET_LOCATION.'dist/');
	define('ADM_BOOTSTRAP_PATH',ADM_DIST_LOCATION.'bootstrap/');

	define('ADM_DIST_CSS',ADM_DIST_LOCATION.'css/');
	define('ADM_DIST_JS',ADM_DIST_LOCATION.'js/');
	define('ADM_DIST_IMG',ADM_DIST_LOCATION.'img/');
	define('ADM_PLUGINS',ADM_ASSET_LOCATION.'plugins/');
?>